# aws_timestreamwrite_database
resource "aws_timestreamwrite_database" "iot" {
  database_name = "iot"
}

# aws_timestreamwrite_table linked to the database

resource "aws_timestreamwrite_table" "temperaturesensor" {
  database_name = "iot"
  table_name    = "temperaturesensor"
}