{
    "FunctionName": "ac_control_lambda",
    "FunctionArn": "arn:aws:lambda:eu-west-1:440464562515:function:ac_control_lambda",
    "Runtime": "python3.7",
    "Role": "arn:aws:iam::440464562515:role/lambda_role",
    "Handler": "ac_control_lambda.lambda_handler",
    "CodeSize": 20679190,
    "Description": "",
    "Timeout": 3,
    "MemorySize": 128,
    "LastModified": "2023-02-28T14:43:54.000+0000",
    "CodeSha256": "yi/mNRHbxFKyZkvXMW3l6Uz1kkV22sXsSLUDvBW+/dM=",
    "Version": "$LATEST",
    "TracingConfig": {
        "Mode": "PassThrough"
    },
    "RevisionId": "5871b999-5cc5-40ea-b981-d5d15391d501",
    "State": "Active",
    "LastUpdateStatus": "InProgress",
    "LastUpdateStatusReason": "The function is being created.",
    "LastUpdateStatusReasonCode": "Creating",
    "PackageType": "Zip",
    "Architectures": [
        "x86_64"
    ],
    "EphemeralStorage": {
        "Size": 512
    }
}
